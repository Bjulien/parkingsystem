package com.parkit.parkingsystem.service;

import com.parkit.parkingsystem.constants.Fare;
import com.parkit.parkingsystem.model.Ticket;

public class FareCalculatorService {

    public void calculateFare(Ticket ticket, int recurance){
        if( (ticket.getOutTime() == null) || (ticket.getOutTime().before(ticket.getInTime())) ){
            throw new IllegalArgumentException("Out time provided is incorrect:"+ticket.getOutTime().toString());
        }
       
        
        long duration= ticket.getOutTime().getTime() - ticket.getInTime().getTime();
        
        float hDuration = (float)duration /(1000*60*60);

        switch (ticket.getParkingSpot().getParkingType()){
            case CAR: {
            	if (duration < 30*60*1000) {
            		ticket.setPrice(0);//Price Car parking if less than 30 minutes
            	}
            	else {
            		if (recurance >= 2) {
            			System.out.println("Reccuring customers get a "+Fare.DISCOUNT+"% discount!"); 
            			ticket.setPrice((hDuration * Fare.CAR_RATE_PER_HOUR) - ((hDuration * Fare.CAR_RATE_PER_HOUR)*Fare.DISCOUNT)/100);//Price Car parking with discount
            		}
            		else {
            			ticket.setPrice(hDuration * Fare.CAR_RATE_PER_HOUR);//Price Car parking without discount
            		}
            		
            	}
                
                break;
            }
            case BIKE: {
            	if (duration < 30*60*1000) {
            		ticket.setPrice(0);//Price Bike parking if less than 30 minutes
            	}
            	else {
            		if (recurance >= 2) {
            			System.out.println("Reccuring customers get a "+Fare.DISCOUNT+"% discount!"); 
            			ticket.setPrice((hDuration * Fare.BIKE_RATE_PER_HOUR) - ((hDuration * Fare.BIKE_RATE_PER_HOUR)*Fare.DISCOUNT)/100);//Price Bike parking with discount
            		}
            		else {
            			ticket.setPrice(hDuration * Fare.BIKE_RATE_PER_HOUR);//Price Bike parking without discount
            		}
            	}
                
                break;
            }
            default: throw new IllegalArgumentException("Unkown Parking Type");
        }
    }
}
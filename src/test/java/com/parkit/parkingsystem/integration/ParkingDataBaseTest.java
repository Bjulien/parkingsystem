package com.parkit.parkingsystem.integration;

import com.parkit.parkingsystem.constants.DBConstants;
import com.parkit.parkingsystem.constants.Fare;
import com.parkit.parkingsystem.constants.ParkingType;
import com.parkit.parkingsystem.dao.ParkingSpotDAO;
import com.parkit.parkingsystem.dao.TicketDAO;
import com.parkit.parkingsystem.integration.config.DataBaseTestConfig;
import com.parkit.parkingsystem.integration.service.DataBasePrepareService;
import com.parkit.parkingsystem.model.ParkingSpot;
import com.parkit.parkingsystem.model.Ticket;
import com.parkit.parkingsystem.service.ParkingService;
import com.parkit.parkingsystem.util.InputReaderUtil;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.concurrent.TimeUnit;

@ExtendWith(MockitoExtension.class)
public class ParkingDataBaseTest {

    private static DataBaseTestConfig dataBaseTestConfig = new DataBaseTestConfig();
    private static ParkingSpotDAO parkingSpotDAO;
    private static TicketDAO ticketDAO;
    private static DataBasePrepareService dataBasePrepareService;
    static Connection connect = null;

    @Mock
    private static InputReaderUtil inputReaderUtil;

    @BeforeAll
    private static void setUp() throws Exception{
        parkingSpotDAO = new ParkingSpotDAO();
        parkingSpotDAO.dataBaseConfig = dataBaseTestConfig;
        ticketDAO = new TicketDAO();
        ticketDAO.dataBaseConfig = dataBaseTestConfig;
        dataBasePrepareService = new DataBasePrepareService();
    }

	

    @BeforeEach
    private void setUpPerTest() throws Exception {
        when(inputReaderUtil.readSelection()).thenReturn(1);
        when(inputReaderUtil.readVehicleRegistrationNumber()).thenReturn("ABCDEF");
        dataBasePrepareService.clearDataBaseEntries();
    }

    @AfterAll
    private static void tearDown(){

    }

    @Test
    public void testParkingACar() throws Exception {
    	
    	//check that a ticket is actualy saved in DB and Parking table is updated with availability
    	
        ParkingService parkingService = new ParkingService(inputReaderUtil, parkingSpotDAO, ticketDAO);
        parkingService.processIncomingVehicle();
        
        connect = dataBaseTestConfig.getConnection();
        String request = "SELECT VEHICLE_REG_NUMBER FROM ticket WHERE ID=?";
        PreparedStatement statement = connect.prepareStatement(request);
        statement.setInt(1, 1);
        ResultSet result = statement.executeQuery();
		result.next();
		
		//ticket is in table
		
		assertEquals(result.getString(1), "ABCDEF");

		String request1 = "SELECT AVAILABLE FROM parking WHERE PARKING_NUMBER=?";
		PreparedStatement statement1 = connect.prepareStatement(request1);
		statement1.setInt(1, 1);
		ResultSet result1 = statement1.executeQuery();
		result1.next();
		
		//availability set to 0

		assertEquals(result1.getInt(1),0);
        
    }

    @Test
    public void testParkingLotExit() throws Exception {
    	
    		
    	
			ParkingService parkingService = new ParkingService(inputReaderUtil, parkingSpotDAO, ticketDAO);
			parkingService.processIncomingVehicle();
		
			TimeUnit.MILLISECONDS.sleep(1000);
			parkingService.processExitingVehicle();
			
			//check that the fare generated and out time are populated correctly in the database

			connect = dataBaseTestConfig.getConnection();

			String request2 = "SELECT PRICE FROM ticket WHERE ID=?";
			PreparedStatement statement2 = connect.prepareStatement(request2);
			statement2.setInt(1, 1);
			ResultSet result2 = statement2.executeQuery();
			result2.next();
			
			//Price generated in table
			
			assertEquals(result2.getDouble(1),ticketDAO.getTicket("ABCDEF").getPrice());

			String request3 = "SELECT OUT_TIME FROM ticket WHERE ID=?";
			PreparedStatement statement3 = connect.prepareStatement(request3);
			statement3.setInt(1, 1);
			ResultSet result3 = statement3.executeQuery();
			result3.next();

			//time in table is generated

			assertEquals(result3.getTimestamp(1),ticketDAO.getTicket("ABCDEF").getOutTime());
		
	        int timeIn = ticketDAO.getTicket("ABCDEF").getInTime().getSeconds();
	        int timeOut = ticketDAO.getTicket("ABCDEF").getOutTime().getSeconds();
	        
	        //time changed
	        
	        assertTrue(timeOut>timeIn);
	        
	        double Price = ticketDAO.getTicket("ABCDEF").getPrice();
	        
	        //Price Exist
	        
	        assertEquals (Price, 0);
	        
	   
        
    }

}
